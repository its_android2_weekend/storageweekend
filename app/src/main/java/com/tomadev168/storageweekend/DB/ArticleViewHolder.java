package com.tomadev168.storageweekend.DB;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.tomadev168.storageweekend.Entity.Article;
import com.tomadev168.storageweekend.R;

class ArticleViewHolder extends RecyclerView.ViewHolder {
    private TextView tvTitle;
    private TextView tvDescription;
    private ImageView ivDelete;

    public ArticleViewHolder(@NonNull View itemView, RecylerViewListener listener) {
        super(itemView);
        tvTitle = itemView.findViewById(R.id.tvTitle);
        tvDescription = itemView.findViewById(R.id.tvDescription);
        ivDelete = itemView.findViewById(R.id.ivDelete);

        ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemDeleteClick(getAdapterPosition());
            }
        });

    }

    public void onBind(Article article) {
        tvTitle.setText(article.getTitle());
        tvDescription.setText(article.getDescription());
    }
}
