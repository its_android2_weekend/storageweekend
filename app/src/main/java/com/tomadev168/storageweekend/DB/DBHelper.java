package com.tomadev168.storageweekend.DB;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.tomadev168.storageweekend.util.DBConstant;

public class DBHelper extends SQLiteOpenHelper {

    public final String TAG = this.getClass().getSimpleName();
    private static DBHelper instance=null;

    public static DBHelper getInstance(Context context){
        if(instance == null){
            instance = new DBHelper(context);
        }
        return instance;
    }

    private DBHelper(Context context) {
        super(context, DBConstant.DATABASE_NAME, null, DBConstant.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //Create Table Article;
        Log.e(TAG,DBConstant.CREATE_ARTICLE_TABLE);
        db.execSQL(DBConstant.CREATE_ARTICLE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        //Skip
    }
}
