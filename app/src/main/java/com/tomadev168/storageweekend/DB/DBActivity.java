package com.tomadev168.storageweekend.DB;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.tomadev168.storageweekend.Entity.Article;
import com.tomadev168.storageweekend.R;

public class DBActivity extends AppCompatActivity implements RecylerViewListener {
    //DB helper;
    ArticleHelper articleHelper;

    //View
    Button btnInsert;
    EditText etTitle;
    EditText etDescription;
    RecyclerView rVArticle;
    //RecylerView Adapter;
    ArticleAdapter articleAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_db);
        articleHelper = ArticleHelper.getInstance(DBActivity.this);//Init Helper;

        initView();
        initEvent();
        SetupRecylerView();
        RefreshData();
    }

    private void RefreshData() {
        articleAdapter.clear();
        articleAdapter.addMoreItems(articleHelper.findAll());
    }

    private void SetupRecylerView() {
        articleAdapter = new ArticleAdapter();
        articleAdapter.setRecylerViewListener(this);
        rVArticle.setLayoutManager(new LinearLayoutManager(this));
        rVArticle.setAdapter(articleAdapter);
    }

    private void initEvent() {
        btnInsert.setOnClickListener(view -> {

            //reading code;
//            List<Article> articleList = articleHelper.findAll();
//            for(Article article: articleList){
//                Log.e("000000",article.toString());
//            }

            String title = etTitle.getText().toString();
            String description = etDescription.getText().toString();

            Article article = new Article(title,description);
            boolean result = articleHelper.insert(article);
            if(result){
                Toast.makeText(this, "Data was inserted", Toast.LENGTH_SHORT).show();
                RefreshData();
            }else{
                Toast.makeText(this, "Data Error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initView() {
        btnInsert = findViewById(R.id.btnInsert);
        etTitle = findViewById(R.id.edTitle);
        etDescription = findViewById(R.id.etDescription);
        rVArticle = findViewById(R.id.rVArticle);
    }

    @Override
    public void onItemDeleteClick(int position) {
        Article article = articleHelper.findAll().get(position);
        if(articleHelper.delete(article.getId())){
            Toast.makeText(this, "Delete Successul", Toast.LENGTH_SHORT).show();
            RefreshData();
        }
        else
            Toast.makeText(this, "Delete Error", Toast.LENGTH_SHORT).show();

    }
}
