package com.tomadev168.storageweekend.DB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.tomadev168.storageweekend.Entity.Article;
import com.tomadev168.storageweekend.util.DBConstant;

import java.util.ArrayList;
import java.util.List;

public class ArticleHelper {
    private DBHelper dbHelper;
    private SQLiteDatabase db;
    private static ArticleHelper instance;

    //SingleTon pattern;
    public static ArticleHelper getInstance(Context context){
        if(instance == null){
            instance = new ArticleHelper(context);
        }
        return instance;
    }

    private ArticleHelper(Context context) {
        dbHelper = DBHelper.getInstance(context);
    }
    public boolean insert(Article article){
        db = dbHelper.getWritableDatabase();

        //Values
        ContentValues values = new ContentValues();
        values.put(DBConstant.ARTICLE_TITLE,article.getTitle());
        values.put(DBConstant.ARTICLE_DESCRIPTION,article.getDescription());

        long result = db.insert(DBConstant.ARTICLE_TABLE,null,values);
        if(result>0)//Insert Success;
            return true;
        return false;
    }
    public List<Article> findAll(){
        List<Article> articleList = new ArrayList<>();
        db = dbHelper.getReadableDatabase();
        Cursor cursor = db.query(DBConstant.ARTICLE_TABLE,
                null,
                null,
                null,
                null,
                null,
                null,
                null
                );
        //Reading Information in Cursor;
        while (cursor !=null && cursor.moveToNext()){
            //read data from cursor;
            int id = cursor.getInt(cursor.getColumnIndex(DBConstant.ARTICLE_ID));
            String title = cursor.getString(cursor.getColumnIndex(DBConstant.ARTICLE_TITLE));
            String description = cursor.getString(cursor.getColumnIndex(DBConstant.ARTICLE_DESCRIPTION));

            //Create Object Article;
            Article article = new Article(id,title,description);
            articleList.add(article);
        }
        return articleList;
    }
    public boolean delete(int id){
        db = dbHelper.getWritableDatabase();
        int result = db.delete(DBConstant.ARTICLE_TABLE,"id=?",new String[]{String.valueOf(id)});
        if(result>0)
            return true;
        return false;
    }
}
