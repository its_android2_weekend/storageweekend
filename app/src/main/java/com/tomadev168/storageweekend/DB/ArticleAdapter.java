package com.tomadev168.storageweekend.DB;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tomadev168.storageweekend.Entity.Article;
import com.tomadev168.storageweekend.R;

import java.util.ArrayList;
import java.util.List;

public class ArticleAdapter extends RecyclerView.Adapter<ArticleViewHolder> {
    private List<Article> articleList;
    private RecylerViewListener listener;

    public ArticleAdapter() {
        articleList = new ArrayList<>();
    }
    public void setRecylerViewListener(RecylerViewListener listener){
        this.listener = listener;
    }

    @NonNull
    @Override
    public ArticleViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        //item row;
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_article,viewGroup,false);
        return new ArticleViewHolder(view,listener);
    }

    @Override
    public void onBindViewHolder(@NonNull ArticleViewHolder holder, int position) {
        //Bind data to each row;
     holder.onBind(articleList.get(position));
    }

    @Override
    public int getItemCount() {
        return articleList.size();
    }
    public void addMoreItems(List<Article> articles){
        this.articleList.addAll(articles);
        notifyDataSetChanged();
    }

    public void clear() {
        this.articleList.clear();
    }
}
