package com.tomadev168.storageweekend.util;

public class DBConstant {
    public static final String DATABASE_NAME = "db_article";
    public static final int DATABASE_VERSION = 1;

    //Entity;
    public static final String ARTICLE_TABLE = "article";
    public static final String ARTICLE_ID = "id";
    public static final String ARTICLE_TITLE = "title";
    public static final String ARTICLE_DESCRIPTION = "description";

    public static final String CREATE_ARTICLE_TABLE = "CREATE TABLE "+
            ARTICLE_TABLE + " ( "+
            ARTICLE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            ARTICLE_TITLE + " TEXT NOT NULL, " +
            ARTICLE_DESCRIPTION + " TEXT NOT NULL )";
}
