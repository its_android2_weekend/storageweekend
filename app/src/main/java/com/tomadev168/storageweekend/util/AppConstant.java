package com.tomadev168.storageweekend.util;

public class AppConstant {
    public static final String USER_NAME_List = "user_name_list";

    public static final String USER_NAME= "user_name";
    public static final String PASSWORD = "password";
    public static final String USER_OBJECT="user_object";

    public static final String MY_FILE= "myfile.txt";
    public static final String MY_PUBLIC_FILE= "myPublic_file.txt";
}

