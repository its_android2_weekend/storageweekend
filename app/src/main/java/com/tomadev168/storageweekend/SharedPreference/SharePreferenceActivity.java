package com.tomadev168.storageweekend.SharedPreference;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.tomadev168.storageweekend.Entity.User;
import com.tomadev168.storageweekend.R;
import com.tomadev168.storageweekend.util.AppConstant;


public class SharePreferenceActivity extends AppCompatActivity {
    Button btnSavePref;
    EditText etText;
    Button btnReadPref;
    TextView tvDisplay;
    Gson gson = new Gson();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_preference);

        //Linking Control Reference
        btnSavePref = findViewById(R.id.btnSaveFilePrivate);
        etText = findViewById(R.id.etText);
        btnReadPref = findViewById(R.id.btnReadFilePrivate);
        tvDisplay = findViewById(R.id.tvDisplay);

        //Assign callback method
        btnSavePref.setOnClickListener(view -> {
            User user = new User(1,"Toma","123");
            saveDataToPrefernce(etText.getText().toString(),user);
        });
        btnReadPref.setOnClickListener(view -> {
            ReadDataPreference();
        });
    }

    private void ReadDataPreference() {
        SharedPreferences sharedPreferences = getSharedPreferences(AppConstant.USER_NAME_List,MODE_PRIVATE);
        String str = sharedPreferences.getString(AppConstant.USER_NAME,"");
        String  userJson = sharedPreferences.getString(AppConstant.USER_OBJECT,"n/a");
        //Convert JSon String back to USER OBJECT;
        User user = gson.fromJson(userJson,User.class);

        //Reading form user Object;
        tvDisplay.setText(str);
        tvDisplay.setText(user.getUserName());
    }


    private void saveDataToPrefernce(String str,User user) {
        SharedPreferences sharedPreferences = getSharedPreferences(AppConstant.USER_NAME_List,MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        //Put your data here;
        editor.putString(AppConstant.USER_NAME,str);
        editor.putString(AppConstant.USER_OBJECT,new Gson().toJson(user));//Convert User object to JSON STRING;
        editor.apply();
    }
}
