package com.tomadev168.storageweekend.File;

import android.Manifest;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.icu.text.Normalizer2;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import com.tomadev168.storageweekend.R;
import com.tomadev168.storageweekend.util.AppConstant;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class StorageActivity extends AppCompatActivity{
    private static final int WRITE_EXTERNAL_STORAGE_CODE = 1;
    Button btnSaveFilePrivate;
    Button btnReadFilePrivate;
    EditText etText;
    TextView tvDisplay;
    Button btnSavePublic;
    Button btnReadPublic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_storage);
        btnSaveFilePrivate = findViewById(R.id.btnSaveFilePrivate);
        btnReadFilePrivate = findViewById(R.id.btnReadFilePrivate);
        tvDisplay = findViewById(R.id.tvDisplay);
        btnSavePublic = findViewById(R.id.btnSavePublic);
        btnReadPublic = findViewById(R.id.btnReadPublic);

        etText = findViewById(R.id.etText);

        //Event Click;
        btnSaveFilePrivate.setOnClickListener(v -> {
            WritePrivateData(etText.getText().toString());//call
        });
        btnReadFilePrivate.setOnClickListener( v -> {
            ReadPrivateData();//call
        });
        btnReadPublic.setOnClickListener( v -> {
            ReadPublicData();//Implement by your self;
        });
        btnSavePublic.setOnClickListener(v -> {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                int check= ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                if(check == PackageManager.PERMISSION_GRANTED){
                    WritePublicData(etText.getText().toString());
                }else{
                    //Request runtime Permission;
                    String[] permission_group = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};
                    requestPermissions(permission_group,WRITE_EXTERNAL_STORAGE_CODE);
                }
            }else{
                //Android version Lollipop
                WritePublicData(etText.getText().toString());
            }


        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == WRITE_EXTERNAL_STORAGE_CODE){
            if(grantResults[0]== PackageManager.PERMISSION_GRANTED){
                WritePublicData(etText.getText().toString());
            }
        }
    }

    private void WritePublicData(String str) {
        File path = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),AppConstant.MY_PUBLIC_FILE);
        FileOutputStream fos=null;
        try {
            fos = new FileOutputStream(path);
            fos.write(str.getBytes());
            fos.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(fos!=null){
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    private void ReadPublicData() {
        FileInputStream fis=null;
        StringBuilder sb = new StringBuilder();
        File path = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),AppConstant.MY_PUBLIC_FILE);
        try {
            fis = new FileInputStream(path);

            int c;
            while ((c= fis.read()) != -1){
                char character = (char)c;
                sb.append(character);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(fis!=null){
                try {
                    fis.close();
                    tvDisplay.setText(sb.toString());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     *
     */
    private void ReadPrivateData() {
        FileInputStream fis=null;
        StringBuilder sb = new StringBuilder();
        try {
            fis = openFileInput(AppConstant.MY_FILE);

            int c;
            while ((c= fis.read()) != -1){
                char character = (char)c;
                sb.append(character);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(fis!=null){
                try {
                    fis.close();
                    tvDisplay.setText(sb.toString());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     *
     * @param str
     */
    private void WritePrivateData(String str) {
        FileOutputStream fos=null; //Use for write data to file
        try {
            fos = openFileOutput(AppConstant.MY_FILE, MODE_PRIVATE); //for wite data;
            fos.write(str.getBytes());
            fos.flush();//save data in files;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(fos!=null){
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
