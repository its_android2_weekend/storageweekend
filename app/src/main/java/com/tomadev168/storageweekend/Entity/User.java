package com.tomadev168.storageweekend.Entity;

public class User {
    private int id;
    private String userName;
    private String Password;

    public String getPassword() {
        return Password;
    }

    public User(int id, String userName, String password) {
        this.id = id;
        this.userName = userName;
        Password = password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
